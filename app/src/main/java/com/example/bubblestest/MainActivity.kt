package com.example.bubblestest

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Icon
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.show_notification_button

private const val NOTIFICATION_CHANNEL_ID = "bubbles_test_channel"
private const val NOTIFICATION_ID = 0

@RequiresApi(Build.VERSION_CODES.O)
class MainActivity : AppCompatActivity() {

    private val icon by lazy {
        Icon.createWithResource(
            this,
            R.drawable.ic_attachment_black_24dp
        )
    }

    private val bubbleIntent: PendingIntent by lazy {
        val target = Intent(this, BubbleActivity::class.java)
        PendingIntent.getActivity(this, 0, target, 0)
    }

    private val notificationManager: NotificationManager by lazy {
        (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).also { notificationManager ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val importance = NotificationManager.IMPORTANCE_HIGH
                val notificationChannel =
                    NotificationChannel(NOTIFICATION_CHANNEL_ID, "Reminder Alarms", importance)
                notificationManager.createNotificationChannel(notificationChannel)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        show_notification_button.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                showTheBubble()
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun showTheBubble() {
        val notification = createBubbleNotification(createBubbleMetaData())
        notificationManager.showNotification(notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createBubbleMetaData() =
        Notification.BubbleMetadata.Builder()
            .setIcon(icon)
            .setIntent(bubbleIntent)
            .setDesiredHeight(350)
            //.setAutoExpandBubble(true) // Bubble activity is automatically expanded when bubble showed. Doesn't work right at the moment - window is displaced
            .setSuppressNotification(true) // Do not show notification in the statusbar (not sure why would you want to)
            .build()

    @RequiresApi(Build.VERSION_CODES.P)
    private fun createBubbleNotification(
        bubbleMetadata: Notification.BubbleMetadata
    ) =
        Notification.Builder(this, NOTIFICATION_CHANNEL_ID)
            .setContentTitle("Bubble test here")
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setBubbleMetadata(bubbleMetadata)
            .build()

    private fun NotificationManager.showNotification(notification: Notification) {
        notify(NOTIFICATION_ID, notification)
    }

}
