package com.example.bubblestest

import android.os.Bundle
import android.view.animation.LinearInterpolator
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_bubble.*

class BubbleActivity : AppCompatActivity() {

    private var clickSuccessfulVisible = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bubble)
        setUpViews()
    }

    private fun setUpViews() {
        button.setOnClickListener {
            clickSuccessfulVisible = !clickSuccessfulVisible
            val newAlpha = if (clickSuccessfulVisible) 1f else 0f
            click_successful_view.animate().apply {
                interpolator = LinearInterpolator()
                duration = 500
                alpha(newAlpha)
                start()
            }
        }
    }
}